# Diantre !
## vim plugin to run tasks
*The word "diantre" means devil in old french and is used like "damn!", the best translation is probably "deuce" in obsolete english.*

## Installation

Diantre is following the standards of Vim so proceed like any other plugin for installation.

## Principles

The core idea of diantre is to automatically tranform vimscript functions into vim commands, it relies on autoload mecanism.
Each **task is a function** that have a **context** and a **group**:
    - the **context** is the name of the **directory** inside autoload where the file that contains the function belongs.
    - the **group** is the **file** where the task is stored.

This organization prevents name collision, providing namespace for all kind of task while giving semantics structures to build practical tools.
An example is more explicit, so you may want to control some service directly from vim like executing actions on a server: start, stop, restart... this is going to be our tasks. 
There's dozens of servers, apache, nginx, bind... this is going to be our context, and finally servers have differents attributes you may want to manipulate, like configuration, the server itself... these are going to be our groups.
The logical way to start a server is to tell "start nginx server" and this is exactly what the command will look like in diantre. 
Literally if you type `:Diantre start nginx server`, it will call the function nginx#server#start stored in autoload/nginx/server.vim
You may want to edit configuration, just write a function `nginx#configuration#edit` inside autoload/nginx/configuration.vim and you'll have made the command 
`:Diantre edit nginx configuration`

Ok it's far to be terse, but thanks to vim you know a bit about context.
For example if you're editing a python script, you can load "python" context with autocommands, so if you have a task to launch python repl, like ":Diantre start python repl", with autocommand, if the buffer is a python file, you'll just have to type `:di start repl`, "di" is a vim abbreviation, it will be replaced by "Diantre" automatically, and context will be found automatically too. You can also preset group to shorter the commands, and many other features like automatic autocompletion, context, group and task manipulations (copy, move, rm, edit...), task versions, task arguments and their completion functions. 

It's still under development, next step is to improve documentation et develop a plugin manager, if you have any suggestion feel free to contribute.
