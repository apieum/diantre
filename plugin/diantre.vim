if exists('g:loaded_diantre')
    finish
endif
let g:loaded_diantre = v:true

function! s:contains(container, element)
    return match(a:container, '^' . a:element . '$') != -1
endfunction
function! s:uniq(list)
    let l:_list = []
    for item in a:list
        if !s:contains(l:_list, item)
            call add(l:_list, item)
        endif 
    endfor
    return sort(l:_list)
endfunction

function! s:init()
    let l:buffer_was_open = v:false
    let l:root = expand('<sfile>:p:h')
    let l:tasks_cache = expand(l:root . '/cache/tasks.cache')
    let l:contexts = []
    let l:groups = {}
    let l:tasks = {}
    
    function! s:split_version(task, ...)
        let l:task = split(a:task, '__')
        return [l:task[0], get(l:task, 1, get(a:000, 0, 0))]
    endfunction

    function! s:format(words)
        return join(s:uniq(a:words), " \n") . ' '
    endfunction

    function! s:contexts(...) closure
        if a:0 == 0
            return copy(l:contexts)
        endif
        return s:uniq(keys(get(l:tasks, a:1, {})))
    endfunction

    function! s:tasks(...) closure
        let l:task = get(a:000, 0, v:false)
        if l:task == v:false
            return copy(l:tasks)
        endif 
        return get(l:tasks, l:task, {})
    endfunction

    function! s:new_context(context) closure
        let l:directory = s:directory(a:context)
        let l:not_exists = !isdirectory(l:directory)
        if l:not_exists 
            call mkdir(l:directory, 'p')
            call s:add_context(a:context) 
        endif
        return l:not_exists
    endfunction

    function! s:add_context(context) closure
        if !s:contains(l:contexts, a:context)
            let l:contexts = sort(add(l:contexts, a:context))
        end
    endfunction

    function! s:del_context(context, ...) closure
        let l:directory = s:directory(a:context)
        if get(a:000, 0, v:false) == v:true && !empty(globpath(l:directory, '*.vim'))
            return v:false
        endif
        if isdirectory(l:directory)
            call delete(l:directory, 'rf')
        endif
        for task in keys(l:tasks)
            for group in keys(get(l:tasks[task], a:context, {}))
                call s:del_task(task, a:context, group, v:false)
            endfor
        endfor
        call s:set_tasks(l:tasks)
        if has_key(l:groups, a:context)
            unlet l:groups[a:context]
        endif
        call filter(l:contexts, 'v:val !=# "' . a:context . '"')
    endfunction

    function s:groups(context, ...) closure
        return get(l:groups, a:context, get(a:000, 0, []))
    endfunction

    function! s:new_group(context, group) closure
        call s:new_context(a:context)
        let l:created = !filereadable(s:file_name(a:context, a:group))
        call s:edit_group(a:context, a:group)
        write!
        call s:add_group(a:context, a:group)
        return l:created
    endfunction

    function! s:edit_group(context, group) closure
        let l:buffer_was_open = bufloaded(s:file_name(a:context, a:group)) 
        execute 'edit! ' . s:file_name(a:context, a:group)
    endfunction

    function! s:close_group(context, group, force) closure
        let l:fname = s:file_name(a:context, a:group)
        update! l:fname
        if a:force || !l:buffer_was_open
            execute 'silent! bdelete! ' . l:fname 
        endif
    endfunction

    function! s:add_group(context, group) closure
        call s:add_context(a:context)
        let l:groups[a:context] = s:groups(a:context)
        if !s:contains(l:groups[a:context], a:group)
            let l:groups[a:context] = sort(add(l:groups[a:context], a:group))
        end
    endfunction

    function! s:group_func_name(context, group) abort
        return a:context . '#' . a:group
    endfunction

    function! s:group_empty(context, group) closure
        return getfsize(g:diantre.file_name(a:context, a:group)) < 1
    endfunction

    function! s:del_group(context, group, ...) closure
        if get(a:000, 0, v:false) == v:true && !s:group_empty(a:context, a:group) 
            return v:false
        endif
        call delete(s:file_name(a:context, a:group))
        for task in keys(l:tasks)
            call s:del_task(task, a:context, a:group, v:false)
        endfor
        call s:set_tasks(l:tasks)
        if has_key(l:groups, a:context)
            call filter(l:groups[a:context], 'v:val !=# "' . a:group . '"')
        endif
        call s:del_context(a:context, v:true) 
    endfunction

    function! s:set_tasks(tasks) closure
        let l:tasks = a:tasks
        call s:save_tasks()
    endfunction

    function! s:save_tasks() closure
        let l:_tasks = []
        for l:task in keys(l:tasks)
            for l:context in keys(l:tasks[l:task])
                for l:group in keys(l:tasks[l:task][l:context])
                    for l:version in keys(l:tasks[l:task][l:context][l:group])
                        call add(l:_tasks, l:task . '__' . l:version . ' ' . l:context . ' ' . l:group . ' ' . l:tasks[l:task][l:context][l:group][l:version])
                    endfor
                endfor
            endfor
        endfor
        call writefile(l:_tasks, l:tasks_cache)
    endfunction

    function! s:add_task(task, context, group, funcname, ...) closure
        call s:add_group(a:context, a:group)
        let [l:task, l:version] = s:split_version(a:task)
        let l:tasks[l:task] = get(l:tasks, l:task, {})
        let l:tasks[l:task][a:context] = get(l:tasks[l:task], a:context, {})
        let l:tasks[l:task][a:context][a:group] = get(l:tasks[l:task][a:context], a:group, {})
        let l:tasks[l:task][a:context][a:group][l:version] = a:funcname
        if get(a:000, 0, v:true) == v:true
            call s:save_tasks()
        endif
    endfunction

    function! s:task_exists(task, context, group) closure
        return has_key(l:tasks, a:task) && has_key(l:tasks[a:task], a:context) && has_key(l:tasks[a:task][a:context], a:group)
    endfunction

    function! s:del_task(task, context, group, ...) closure
        let [l:task, l:version] = s:split_version(a:task, -1)
        if !s:task_exists(a:task, a:context, a:group)
            return v:false
        endif
        if l:version == -1
            let l:tasks[l:task][a:context][a:group] = {}
        else
            unlet! l:tasks[l:task][a:context][a:group][l:version]
        endif
        if empty(l:tasks[a:task][a:context][a:group])
            unlet! l:tasks[a:task][a:context][a:group]
            if empty(l:tasks[a:task][a:context])
                unlet! l:tasks[a:task][a:context]
                if empty(l:tasks[a:task])
                    unlet! l:tasks[a:task]
                endif
            endif
        endif
        if get(a:000, 0, v:true) != v:false
            call s:set_tasks(l:tasks)
        endif
        return v:true
    endfunction

    function! s:directory(context) closure
        return expand(l:root . '/autoload/' . a:context)
    endfunction

    function! s:directories() abort
        return map(split(s:directory('*'), '\n'), 'fnamemodify(v:val, ":t:r")')
    endfunction

    function! s:file_name(context, group) abort
        return s:directory(a:context) . '/' . a:group . '.vim' 
    endfunction

    function! s:files_name(context) abort
        return map(split(globpath(s:directory(a:context), '*.vim'), '\n'), 'fnamemodify(v:val, ":t:r")')
    endfunction

    for line in readfile(l:tasks_cache)
        let [l:fulltask, l:context, l:group, l:funcname] = split(line)
        let [l:task, l:version] = s:split_version(l:fulltask)
        call s:add_task(l:task, l:context, l:group, l:funcname, l:version, v:false)
    endfor

    let g:diantre = {}
    let g:diantre['set_tasks'] = function('s:set_tasks')
    let g:diantre['new_context'] = function('s:new_context') 
    let g:diantre['add_context'] = function('s:add_context') 
    let g:diantre['new_group'] = function('s:new_group') 
    let g:diantre['edit_group'] = function('s:edit_group') 
    let g:diantre['close_group'] = function('s:close_group') 
    let g:diantre['add_group'] = function('s:add_group') 
    let g:diantre['add_task'] = function('s:add_task') 
    let g:diantre['del_context'] = function('s:del_context') 
    let g:diantre['del_group'] = function('s:del_group') 
    let g:diantre['del_task'] = function('s:del_task') 
    let g:diantre['save_tasks'] = function('s:save_tasks') 
    let g:diantre['contexts'] = function('s:contexts')
    let g:diantre['groups'] = function('s:groups')
    let g:diantre['tasks'] = function('s:tasks')
    let g:diantre['directory'] = function('s:directory')
    let g:diantre['directories'] = function('s:directories')
    let g:diantre['file_name'] = function('s:file_name')
    let g:diantre['files_name'] = function('s:files_name')
    let g:diantre['split_version'] = function('s:split_version')
    let g:diantre['format'] = function('s:format')
    return g:diantre 
endfunction

call s:init()

function! Diantre_unset_buffer_contexts()
    unlet! b:diantre_contexts
endfunction
function! Diantre_unset_contexts()
    unlet! g:diantre_contexts
endfunction
function! Diantre_unset_buffer_groups()
    unlet! b:diantre_groups
endfunction
function! Diantre_unset_groups()
    unlet! g:diantre_groups
endfunction

function! Diantre_set_buffer_contexts(...)
    let b:diantre_contexts = a:000
endfunction
function! Diantre_set_contexts(...)
    let g:diantre_contexts = a:000
endfunction
function! Diantre_set_buffer_groups(...)
    let b:diantre_groups = a:000
endfunction
function! Diantre_set_groups(...)
    let g:diantre_groups = a:000
endfunction

function! Diantre_buffer_contexts()
    if exists('b:diantre_contexts')
        return copy(b:diantre_contexts)
    endif
    return []
endfunction
function! Diantre_contexts()
    if !exists('g:diantre_contexts')
        let g:diantre_contexts = ['diantre']
    endif
    return copy(g:diantre_contexts)
endfunction
function! Diantre_all_contexts()
    return Diantre_buffer_contexts() + Diantre_contexts()
endfunction
function! Diantre_buffer_groups()
    if exists('b:diantre_groups')
        return copy(b:diantre_groups)
    endif
   return []
endfunction
function! Diantre_groups()
    if exists('g:diantre_groups')
        return copy(g:diantre_groups)
    endif
    return []
endfunction
function! Diantre_all_groups()
    return Diantre_groups() + Diantre_buffer_groups()
endfunction

runtime plugin/diantre/command.v
