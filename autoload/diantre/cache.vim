
function! diantre#cache#build() abort
    let l:tasks = {}
    call g:diantre.set_tasks(l:tasks)
    for l:context in g:diantre.directories()
        for l:group in g:diantre.files_name(l:context)
            for l:fulltask in diantre#task#list(l:context, l:group)
                let l:funcname = l:context . '#' . l:group . '#' . l:fulltask
                call g:diantre.add_task(l:fulltask, l:context, l:group, l:funcname, v:false)
            endfor
        endfor
    endfor
    call g:diantre.save_tasks()
endfunction


