
function! diantre#complete#new(task, context, group) abort
    let l:tname = a:task . '__complete'
    if diantre#task#exists(l:tname, a:context, a:group)
        return diantre#complete#edit(a:task, a:context, a:group)
    endif
    call diantre#task#new(l:tname, a:context, a:group, 'words', 'cur_pos', '...')
    call diantre#task#edit(l:tname, a:context, a:group)
    normal! 's
    call append(line('.'), ["\tlet l:result = get(a:000, 0, [])"])
    update!
endfunction

function! diantre#complete#edit(task, context, group) abort
    return diantre#task#edit(a:task . '__complete', a:context, a:group)
endfunction

function! diantre#complete#close(context, group, ...) abort
    let l:force = get(a:000, 0, v:true)
    return diantre#task#close(a:context, a:group, l:force)
endfunction

function! diantre#complete#yank(task, context, group) abort
    return diantre#task#yank(a:task . '__complete', a:context, a:group)
endfunction

function! diantre#complete#rename(task, context, group, newtask) abort
    let l:tname = a:newtask . '__complete'
    if !diantre#task#exists(l:tname, a:context, a:group)
        return diantre#task#rename(a:task . '__complete', a:context, a:group, l:tname, v:false)
    endif
    echoerr('[rename] Complete function already exists for task:' . a:newtask . ' ' . a:context . ' ' . a:group)
endfunction

function! diantre#complete#chgrp(task, context, group, newgroup) abort
    let l:tname = a:task . '__complete'
    if !diantre#task#exists(l:tname, a:context, a:newgroup)
        return diantre#task#chgrp(l:tname, a:context, a:group, a:newgroup, v:false)
    endif
    echoerr('[chgrp] Complete function already exists for task:' . a:task . ' ' . a:context . ' ' . a:newgroup)
endfunction

function! diantre#complete#chdir(task, context, group, newcontext) abort
    let l:tname = a:task . '__complete'
    if !diantre#task#exists(l:tname, a:newcontext, a:group)
        return diantre#task#chdir(l:tname, a:context, a:group, a:newcontext, v:false)
    endif
    echoerr('[chdir] Complete function already exists for task:' . a:task . ' ' . a:newcontext . ' ' . a:group)
endfunction

function! diantre#complete#move(task, context, group, newtask, newcontext, newgroup) abort
    let l:tname = a:newtask . '__complete'
    if !diantre#task#exists(l:tname, a:newcontext, a:newgroup)
        return diantre#task#move(a:task . '__complete', a:context, a:group, l:tname, a:newcontext, a:newgroup, v:false)
    endif
    echoerr('[move] Complete function already exists for task:' . a:newtask . ' ' . a:newcontext . ' ' . a:newgroup)
endfunction

function! diantre#complete#copy(task, context, group, newtask, newcontext, newgroup) abort
    let l:tname = a:newtask . '__complete'
    if !diantre#task#exists(l:tname, a:newcontext, a:newgroup)
        return diantre#task#copy(a:task . '__complete', a:context, a:group, l:tname, a:newcontext, a:newgroup, v:false)
    endif
    echoerr('[copy] Complete function already exists for task:' . a:newtask . ' ' . a:newcontext . ' ' . a:newgroup)
endfunction

function! diantre#complete#remove(task, context, group) abort
    call diantre#task#remove(a:task . '__complete', a:context, a:group, v:false)
endfunction
