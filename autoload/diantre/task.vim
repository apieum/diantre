
function! s:func_name(context, group, task) abort
    return a:context . '#' . a:group . '#' . a:task
endfunction
function! s:next_func_name(context, group, task) abort
    let l:version = s:next_version(a:context, a:group, a:task)
    let l:fname = s:func_name(a:context, a:group, a:task) . (l:version !=0 ? '__' . l:version : '')
    return l:fname
endfunction

function! s:func_pattern(context, group, task) abort
    return 'function[!][^\s]\+' . s:func_name(a:context, a:group, a:task).'\(__[0-9]\+\)\?('
endfunction

function! s:next_version(context, group, task) abort
    if !diantre#task#edit(a:task, a:context, a:group)
        call diantre#task#close(a:context, a:group, v:false)
        return 0
    endif
    let l:lines=[]
    execute 'silent! %s/' . s:func_pattern(a:context, a:group, a:task) . '/\=add(l:lines, stridx(submatch(1), "_") == 0 ? submatch(1)[2:-1] : 0)/gn'
    call diantre#task#close(a:context, a:group, v:false)
    return max(l:lines) + 1
endfunction

function! diantre#task#new(task, context, group, ...) abort
    call g:diantre.new_group(a:context, a:group)
    let l:version = s:next_version(a:context, a:group, a:task)
    let l:task_name = a:task . (l:version !=0 ? '__' . l:version : '')
    let l:fname = s:func_name(a:context, a:group, l:task_name)
    execute 'edit! ' . g:diantre.file_name(a:context, a:group)
    call append(line('$'), ['', 'function! ' . l:fname . '(' . join(a:000, ', ') . ') abort', "\t", 'endfunction'])
    call cursor(line('$')-1, '$')
    update!
    call g:diantre.add_task(l:task_name, a:context, a:group, l:fname)
endfunction

function! diantre#task#edit(task, context, group) abort
    call g:diantre.edit_group(a:context, a:group)
    let l:found = search(s:func_pattern(a:context, a:group, a:task))
    if l:found
        " add a marker s for the start of task
        normal! ms
        /^endfunction
        " add a marker e for the end of task and go to last line
        normal! mek$
    endif
    return l:found
endfunction

function! diantre#task#close(context, group, ...) abort
    return g:diantre.close_group(a:context, a:group, get(a:000, 0, v:true))
endfunction

function! diantre#task#remove(task, context, group, ...) abort
    if get(a:000, 0, v:false) !=? v:false
        call diantre#complete#remove(a:task, a:context, a:group)
    endif
    let l:found = diantre#task#edit(a:task, a:context, a:group) 
    if l:found 
        normal! 's
        while getline(line('.') - 1) ==? '' && line('.') > 1
            normal! kdd
        endwhile
        normal! d'e
        update!
        call g:diantre.del_group(a:context, a:group, v:true)
    endif
    call diantre#task#close(a:context, a:group, v:false)
    call g:diantre.del_task(a:task, a:context, a:group)
    return l:found 
endfunction

function! diantre#task#yank(task, context, group) abort
    let l:found = diantre#task#edit(a:task, a:context, a:group) 
    if l:found 
        normal! 'sy'e
    endif
    call diantre#task#close(a:context, a:group, v:false)
    return l:found 
endfunction

function! diantre#task#rename(task, context, group, newtask, ...) abort
    if get(a:000, 0, v:false) !=? v:false
        call diantre#complete#rename(a:task, a:context, a:group, a:newtask)
    endif
    let l:pattern = s:func_pattern(a:context, a:group, a:task)
    let l:version = s:next_version(a:context, a:group, a:newtask)
    let l:found = diantre#task#edit(a:task, a:context, a:group) 
    if l:version == 0 && l:found
        execute "normal! 's0f(cT#" . a:newtask 
        update!
        let l:version = 1
    endif
    while search(l:pattern, 'cz') != 0
        execute 'normal! 0f(cT#' . a:newtask . '__' . l:version
        let l:version+= 1
        update!
    endwhile
    call diantre#task#close(a:context, a:group, v:false)
endfunction

function! diantre#task#chgrp(task, context, group, newgroup, ...) abort
    return diantre#task#move(a:task, a:context, a:group, a:task, a:context, a:newgroup, get(a:000, 0, 0))
endfunction

function! diantre#task#chdir(task, context, group, newcontext, ...) abort
    return diantre#task#move(a:task, a:context, a:group, a:task, a:newcontext, a:group, get(a:000, 0, 0))
endfunction

function! diantre#task#move(task, context, group, newtask, newcontext, newgroup, ...) abort
    if get(a:000, 0, v:false) !=? v:false
        call diantre#complete#move(a:task, a:context, a:group, a:newtask, a:newcontext, a:newgroup)
    endif
    call g:diantre.new_group(a:newcontext, a:newgroup)
    let l:fname = s:next_func_name(a:newcontext, a:newgroup, a:newtask)
    if diantre#task#remove(a:task, a:context, a:group)
        call g:diantre.edit_group(a:newcontext, a:newgroup)
        normal! Go
        normal! Gp
        execute 's/' . a:context . '#' . a:group . '#' . a:task . '[^\(]*(/' . l:fname . '(/g'
        update!
    endif
endfunction

function! diantre#task#copy(task, context, group, newtask, newcontext, newgroup, ...) abort
    if get(a:000, 0, v:false) !=? v:false
        call diantre#complete#copy(a:task, a:context, a:group, a:newtask, a:newcontext, a:newgroup)
    endif
    call g:diantre.new_group(a:newcontext, a:newgroup)
    let l:fname = s:next_func_name(a:newcontext, a:newgroup, a:newtask)
    if diantre#task#yank(a:task, a:context, a:group)
        call g:diantre.edit_group(a:newcontext, a:newgroup)
        normal! Go
        normal! Gp
        execute 's/' . a:context . '#' . a:group . '#' . a:task . '[^\(]*(/' . l:fname . '(/g'
        update!
    endif
endfunction

function! diantre#task#exists(task, context, group) abort
    let l:fname = g:diantre.file_name(a:context, a:group)
    let l:found = v:false
    if filereadable(l:fname)
        let l:found = diantre#task#edit(a:task, a:context, a:group)
        call diantre#task#close(a:context, a:group, v:false)
    endif
    return l:found
endfunction

function! diantre#task#list(context, group) abort
    call g:diantre.edit_group(a:context, a:group)
    let l:lines=[]
    execute 'silent! %s/function[!][^\s]\+' . a:context . '#' . a:group . '#\([^\(]\+\(__[0-9]\+\)\?\)(/\=stridx(submatch(1), "__complete") == -1 ? add(l:lines, submatch(1)) : ""/gn'
    call diantre#task#close(a:context, a:group, v:false)
    return l:lines
endfunction
