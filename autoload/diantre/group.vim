function! diantre#group#new(context, group) abort
    return g:diantre.new_group(a:context, a:group)
endfunction

function! diantre#group#edit(context, group) abort
    return g:diantre.edit_group(a:context, a:group)
endfunction

function! diantre#group#close(context, group, ...) abort
    return g:diantre.close_group(a:context, a:group, get(a:000, 0, v:true))
endfunction

function! diantre#group#remove(context, group, ...) abort
    call g:diantre.del_group(a:context, a:group, get(a:000, 0, v:false))
endfunction

function! diantre#group#rename(context, group, newgroup, ...) abort
    return diantre#group#move(a:context, a:group, a:context, a:newgroup, get(a:000, 0, v:true))
endfunction

function! diantre#group#chdir(context, group, newcontext, ...) abort
    return diantre#group#move(a:context, a:group, a:newcontext, a:group, get(a:000, 0, v:true))
endfunction

function! diantre#group#move(context, group, newcontext, newgroup, ...) abort
    call diantre#group#copy(a:context, a:group, a:newcontext, a:newgroup, get(a:000, 0, v:true))
    call diantre#group#remove(a:context, a:group)
endfunction

function! s:refactor(content, context, group, newcontext, newgroup)
    let l:tasks = diantre#group#tasks(a:newcontext, a:newgroup)
    let l:content = []
    let l:orig = a:context . '#' . a:group
    let l:dest = a:newcontext . '#' . a:newgroup
    for l:line in a:content
        let l:match = matchlist(l:line, 'function[!][^\s]\+' . l:orig . '#\([^\(]\+\)(')
        let l:task = split(get(l:match, 1, ''), '__')
        if l:match != [] && get(l:task, 1, '') !=? 'complete'
            let l:tasks[l:task[0]] = get(l:tasks, l:task[0], -1) + 1
            let l:version = (l:tasks[l:task[0]] == 0 ? '' : '__' . string(l:tasks[l:task[0]]))
            let l:line = substitute(l:line, l:orig . '#\([^\(]\+\)', l:dest . '#' . l:task[0] . l:version, '')
        endif
        call add(l:content, l:line)
    endfor
    return l:content
endfunction

function! diantre#group#copy(context, group, newcontext, newgroup, ...) abort
    let l:complete = get(a:000, 0, v:true)
    call diantre#group#new(a:newcontext, a:newgroup)
    call diantre#group#edit(a:context, a:group)
    let l:content = s:refactor(getline(0, line('$')), a:context, a:group, a:newcontext, a:newgroup)
    call diantre#group#close(a:context, a:group)
    call diantre#group#edit(a:newcontext, a:newgroup)
    let l:start = line('$')
    call append(l:start, l:content)
    update!
    call diantre#group#close(a:newcontext, a:newgroup)
endfunction

function! diantre#group#merge(context, group, ...) abort
    for group in a:000
        call diantre#group#rename(a:context, group, a:group)
    endfor
endfunction

function! diantre#group#tasks(context, group) abort
    call diantre#group#edit(a:context, a:group)
    let l:tasks={}
    let l:linenr = 0
    let l:last_line = line('$')
    while l:linenr <= l:last_line
        let l:line = getline(l:linenr)
        let l:match = matchlist(l:line, 'function[!][^\s]\+' . a:context . '#' . a:group . '#\([^\(]\+\)(')
        if l:match != [] 
            let l:task = split(get(l:match, 1, ''), '__')
            if get(l:task, 1, '') !=? 'complete' && get(l:tasks, l:task[0], -1) < get(l:task, 1, 0)
                let l:tasks[l:task[0]] = get(l:task, 1, 0)
            endif 
        endif
        let l:linenr +=1
    endwhile
    call diantre#group#close(a:context, a:group, v:false)
    return l:tasks
endfunction

function! diantre#group#set(...) abort
    return call('Diantre_set_groups', uniq(copy(a:000)))
endfunction

function! diantre#group#bset(...) abort
    return call('Diantre_set_buffer_groups', uniq(copy(a:000)))
endfunction

function! diantre#group#insert(...) abort
    return call('Diantre_set_groups', uniq(a:000 + Diantre_groups()))
endfunction

function! diantre#group#binsert(...) abort
    return call('Diantre_set_buffer_groups', uniq(a:000 + Diantre_buffer_groups()))
endfunction

function! diantre#group#append(...) abort
    return call('Diantre_set_groups', uniq(Diantre_groups() + a:000))
endfunction

function! diantre#group#bappend(...) abort
    return call('Diantre_set_buffer_groups', uniq(Diantre_buffer_groups() + a:000))
endfunction

function! diantre#group#unset(...) abort
    if a:0 == 0
        return call('Diantre_unset_groups', [])
    endif
    let l:groups = copy(Diantre_groups())
    for group in a:000
        call filter(l:groups, '"' . group . '"!= v:val')
    endfor
    return call('Diantre_set_groups', l:groups)
endfunction

function! diantre#group#bunset(...) abort
    if a:0 == 0
        return call('Diantre_unset_buffer_groups', [])
    endif
    let l:groups = copy(Diantre_buffer_groups())
    for group in a:000
        call filter(l:groups, '"' . group . '"!= v:val')
    endfor
    return call('Diantre_set_buffer_groups', l:groups)
endfunction
