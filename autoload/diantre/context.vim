
function! diantre#context#exists(context) abort
    return isdirectory(g:diantre.directory(a:context))
endfunction

function! diantre#context#new(context) abort
    call g:diantre.new_context(a:context)
endfunction

function! diantre#context#remove(context) abort
    call g:diantre.del_context(a:context) 
endfunction

function! diantre#context#remove_empty(context) abort
    call g:diantre.del_context(a:context, v:true) 
endfunction

function! diantre#context#move(context, newcontext) abort
    call diantre#context#new(a:newcontext)
    for group in g:diantre.groups(a:context)
        call diantre#group#chdir(a:context, group, a:newcontext)
    endfor
    call diantre#context#remove(a:context)
endfunction

function! diantre#context#copy(context, newcontext) abort
    for group in g:diantre.groups(a:context)
        call diantre#group#copy(a:context, group, a:newcontext, group)
    endfor
endfunction

function! diantre#context#set(...) abort
    return call('Diantre_set_contexts', uniq(copy(a:000)))
endfunction

function! diantre#context#bset(...) abort
    return call('Diantre_set_buffer_contexts', uniq(copy(a:000)))
endfunction

function! diantre#context#insert(...) abort
    return call('Diantre_set_contexts', uniq(a:000 + Diantre_contexts()))
endfunction

function! diantre#context#binsert(...) abort
    return call('Diantre_set_buffer_contexts', uniq(a:000 + Diantre_buffer_contexts()))
endfunction

function! diantre#context#append(...) abort
    return call('Diantre_set_contexts', uniq(Diantre_contexts() + a:000))
endfunction

function! diantre#context#bappend(...) abort
    return call('Diantre_set_buffer_contexts', uniq(Diantre_buffer_contexts() + a:000))
endfunction

function! diantre#context#unset(...) abort
    if a:0 == 0
        return call('Diantre_unset_contexts', [])
    endif
    let l:contexts = Diantre_contexts()
    for context in a:000
        call filter(l:contexts, '"' . context . '"!= v:val')
    endfor
    return call('Diantre_set_contexts', l:contexts)
endfunction

function! diantre#context#bunset(...) abort
    if a:0 == 0
        return call('Diantre_unset_buffer_contexts', [])
    endif
    let l:contexts = Diantre_buffer_contexts()
    for context in a:000
        call filter(l:contexts, '"' . context . '"!= v:val')
    endfor
    return call('Diantre_set_buffer_contexts', l:contexts)
endfunction
