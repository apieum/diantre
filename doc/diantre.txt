*diantre.txt* Simple task runner for vim

       /                                                                        
      ++                                                                        
      $ $        &###@%      %/                       &&                      *@
     /& %        %#####%     #*                       %%                      *#
     &@ #*       %@+++%#+    /                        %%                      *#
    /##+#@       %%    #$                             %%                      *#
    $##%##+      %%    #&  +&&+     +&&&&& +&  $$   /&##&   &* /&*    /&&&+   *#
$@@%//$$*/+@@@+  %%    @&  $##*    +###### *# %##@  /####/  @&/###*  /#####+  *#
 ##$ $& %+ %#%   %%    @&  +$#*    @#&$$%# *#$#&%#$  $@@$   @&@@$##  %#&$&#@  *#
 $#/&#/ +#*+#+   %%    @&    #*    #$   *# *##$  @%   %%    @##/ +#+ #&   $#  *#
  &%#$   %#$&    %%    @&    #*   /#+   *# *#@   &@   %%    @#*   &+ #&***&#/ *#
  *##+   *##/    %%    @&    #*   /#+   *# *#/   &@   %%    @%      /#######/ *#
   %#%   @#&     %%    @&    #*   /#+   @# *#    &@   %%    @&      /#@%%%%%/ +%
   +##+ $##      %%    #$    #*   /#*  &## *#    &@   %@    @&       #$         
    %#@ ##$      %%///&#+   /#$    #@/$#%# *#    &@   $#*/  @&       @@/////    
    /##@#@       %#####@   $###@   $###&*# *#    &@    ###& @&       *######/ +#
     &###*       %####@/   $###@    &#% *# *#    &@    +@#& @$        $#####/ +@
      ##@                                                                       
      $#+                                                                       
       &                                                                        
                                                                                
===============================================================================
CONTENTS                                                        *DiantreContent*

    1. Introduction ................ |DiantreIntroduction|
    2. Usage ....................... |DiantreUsage|
        2.1 Tasks .................. |DiantreTasks|
        2.2 Completion ............. |DiantreCompletion|
        2.3 Groups ................. |DiantreGroups|
        2.4 Contexts ............... |DiantreContexts|
    3. Configuration................ |DiantreConfiguration|
        3.1 Home Directory ......... |DiantreHome|
        3.2 Presets ................ |Presets|
        3.4 Task versions .......... |TasksVersions|
    4. License ..................... |DiantreLicense|

===============================================================================
1. Introduction                                            *DiantreIntroduction*

The word "diantre" means devil in old french and is used like "damn!". 
The best translation is probably "deuce" in obsolete english.

Diantre is a task runner for vim. The main purpose is to benefit from vim
capabilities to edit files and run external commands.
The core idea is to automatically tranform vimscript functions into vim
commands, it relies on autoload mecanism.

Each task is a vimscript function that have a context and a group:
    - the context is the name of the directory inside autoload where the file
      that contains the function belongs.
    - the group is the file where the task is stored.

This organization prevents name collision, while providing heuristics to build
practical tools.
Some common task you probably use everyday: start a project in some language,
push, test, deploy your code... are made easy with diantre

===============================================================================
2. Usage                                                          *DiantreUsage*

Diantre transforms functions like python#project#start(name) into commands
like: >
    :Diantre start python project <name>

In this example "python" is the context, "project" is the group, and "start"
the task. This function is stored in autoload/python/project.vim

It may looks verbose, but it permits to memorize easily the available
commands, and autocompletion is automatically built for each function inside
Diantre home directory. 
See |DiantreConfiguration| to set your context and group with autocommand and
reduce the verbosity.

NOTE: By default diantre context is set to "diantre" so you don't have to type
it like it's shown in this doc.
For example: >
    :Diantre new diantre task <name> <context> <group> <args> 
If you've not modified configuration, you can just do: >
    :di new task <name> <context> <group> <args> 
Full command line is shown in this doc to prevent misunderstandings, both are
working, and autocompletion will suggest both.

The abbreviation ":di " remove the need of writting ":Diantre " each time,
when you type :space after di in command line "di " will be replaced by
"Diantre ".  
For more information see |:ab| 

This syntax enforce learning of commands by enabling to use common methods
of doing things.
For example you would probably use similar method for any programming
languages, and it's made possible to do: >
    :di start js project <args> 
    :di start nodejs project <args> 
    :di start ruby project <args> 
    ... to name few

With plugins js, nodejs, ruby... you add features specific to your programming
language while sharing similar heuristics for all languages.

You can override tasks, without altering the original, by using
versionned tasks (see |TasksVersions|).



-------------------------------------------------------------------------------
2.1 Tasks                                                 *task* *DiantreTasks*

    new:                                                                 *new* 
            Create a new task and edit it: >
                :Diantre new diantre task <name> <context> <group> <args> 
<            It will create a function <context>#<group>#<name>(<args>) inside
            <group>.vim file in autoload/<context> directory with parameters
            <args> and position the cursor inside the function. If task exists
            it will add a versionned task by adding "__<version>" after the
            function <name>. see |TasksVersions| 
            Version is found by getting the highest version of task and
            adding 1 to it.
        
    edit:                                                               *edit* 
            Open autoload/<context>/<group>.vim and position the cursor inside
            task <name>: >
                :Diantre edit diantre task <name> <context> <group>

<            NOTE: markers 's and 'e are added to the start and end of
            function if task is found and true is returned if function
            exists, false otherwise

   close:                                                              *close* 
            Save and close the group buffer: >
                :Diantre close diantre task <context> <group>
<
  remove:                                                             *remove* 
            Delete task and group if it's the last task, It returns true if
            task was found, false otherwise. You can get the deleted code
            from the unnamed register "": >
                :Diantre remove diantre task <name> <context> <group>
<
    yank:                                                               *yank* 
            Fill unnamed register "", with the code of the function of the
            given task. Used internally for task |copy|: >
                :Diantre yank diantre task <name> <context> <group>
<
  rename:                                                             *rename* 
            Change task name. If a |__complete| function was set for this task
            you can rename it with the task by passing a last argument
            different than v:false. >
            :Diantre rename diantre task <name> <context> <group> <newname>
<           Renaming completion function with task for example: >
           :Diantre rename diantre task <name> <context> <group> <newname> 1
<
   chgrp:                                                              *chgrp* 
            Change task group, by moving the entire task function and its
            |__complete| related function to the new group file if optionnal
            last argument is given. >
            :Diantre chgrp diantre task <name> <context> <group> <newgroup>
            [<complete>!= v:false]
<
   chdir:                                                              *chdir* 
            Change task context, by moving the entire task function and its
            |__complete| related function to the new context directory if
            optionnal last argument is given. >
            :Diantre chdir diantre task <name> <context> <group> <newcontext>
            [<complete>!= v:false]
<
    move:                                                               *move* 
            Move the task and related |__complete| function to a new context,
            new group and/or new task name. This function is used for |chgrp|
            and |chdir|, if last optionnal argument is different than v:false
            it moves __complete function too: >
            :Diantre move diantre task <name> <context> <group> <newname>
            <newcontext> <newgroup> [<complete>!= v:false]
<
    copy:                                                               *copy* 
            Copy the task and related |__complete| function to a new context,
            new group and/or new task name. Like |move| if a last optionnal
            argument is given |__complete| function is copied too >
            :Diantre copy diantre task <name> <context> <group> <newname>
            <newcontext> <newgroup> [<complete>!= v:false]

<NOTE: |rename|, |chgrp|, |move|, |copy| do not erase the destination task if
it exists, instead it versions the new task. See |TasksVersions| for more
information.

-------------------------------------------------------------------------------
2.2 Completion                                 *__complete* *DiantreCompletion*

The completion system of diantre, is able to look at |DiantreHome| directory and
find all tasks, contexts and groups it contains. It also uses environment
variables for preset contexts (|PresetContexts|) and groups (|PresetGroups|).

Tasks often require parameters and to provide completion for it, the
completion system will call the function with task name ended by "__complete".
If the function doesn't exists nothing is returned. Diantre "complete" group
gives methods similar to tasks to manipulate complete functions. 

Diantre completion functions receive 3 parameters: words, cursor position, and
previous result. It's quite similar to vim default completion system, except,
words is a list, cursor position is the index in this list where the cursor is
located and result is an array with already found completions. The function
should return an array with all possible completions, this array is
automatically formatted to suit vim completion system, which will get rid of
partial words. You have an example of such function in: >
    test/taskcontext/taskgroup.vim
<
     new:                                                       *complete-new* 
            Create new completion function for task named <name>:  >
                :Diantre new diantre complete <name> <context> <group>
<           NOTE: previous result is passed as optionnal argument, and for
           compatibility reasons, it's set with variables arguments. New
           complete functions, will have a line to get this argument.

    edit:                                                      *complete-edit* 
            Open autoload/<context>/<group>.vim and position the cursor inside
            task <name> completion function if it exists: >
                :Diantre edit diantre complete <name> <context> <group>

<            NOTE: Like for tasks markers 's and 'e are added to the start and 
            end of function if it's found and true is returned if function
            exists, false otherwise

   close:                                                     *complete-close* 
            Save and close the group buffer similarly to tasks: >
                :Diantre close diantre complete <context> <group>
<
  remove:                                                    *complete-remove* 
            Delete completion function for task <name>. It returns true if
            function was found, false otherwise. You can get the deleted code
            from the unnamed register "": >
                :Diantre remove diantre complete <name> <context> <group>
<
    yank:                                                      *complete-yank* 
            Fill unnamed register "", with the code of the completion function
            of the given task. Similar to task |yank|: >
                :Diantre yank diantre complete <name> <context> <group>
<
  rename:                                                    *complete-rename* 
            Change task complete function name. Similar to task |rename|: >
            :Diantre rename diantre complete <name> <context> <group> <newname>
<
   chgrp:                                                     *complete-chgrp* 
            Change task complete function group. Similar to task |chgrp|: >
            :Diantre chgrp diantre complete <name> <context> <group> <newgroup>
<
   chdir:                                                     *complete-chdir* 
            Change task complete function context. Similar to task |chdir|: >
            :Diantre chdir diantre complete <name> <context> <group> <newcontext>
<
    move:                                                      *complete-move* 
            Move the task complete function to a new context, new group and/or
            new task name. Similar to task |move|: >
            :Diantre move diantre complete <name> <context> <group> <newname>
            <newcontext> <newgroup>
<
    copy:                                                      *complete-copy* 
            Copy the task complete function to a new context, new group and/or
            new task name. Similar to task |copy|: >
            :Diantre copy diantre complete <name> <context> <group> <newname>
            <newcontext> <newgroup>
-------------------------------------------------------------------------------
2.3 Groups                                                      *DiantreGroups*

     new:                                                           *group-new* 
            Make the file |DiantreHome|/<context>/<group>.vim to create a new
            group and open it: >
                :Diantre new diantre group <context> <group>
<
  remove:                                                        *group-remove* 
            Delete permanently the file |DiantreHome|/<context>/<group>.vim if
            context is empty delete the directory too: >
                :Diantre remove diantre group <context> <group>
< 
  rename:                                                        *group-rename* 
            Rename the group <group> to <newgroup> and the tasks it contains.
            It uses |group-move| and support an optional parameter <complete>
            to disable the renaming of completion tasks. This paramater should
            be strictly equal to |v:false| otherwise it's considered to be true: >
                :Diantre rename diantre group <context> <group> <newgroup>
                [<complete>]
<
   chdir:                                                         *group-chdir* 
            Move the group from <context> directory to <newcontext>. Like
            |group-rename| it uses |group-move| and support an optional
            parameter <complete> to disable the move of completion tasks. This
            paramater should strictly be equal to v:false otherwise it's
            considered to be true: >
                :Diantre chdir diantre group <context> <group> <newcontext>
                [<complete>]
<
    move:                                                          *group-move* 
            Move all tasks from group <group> to <newgroup> from context
            <context> to <newcontext>. If the optional parameter <complete> is
            given and strictly equals to |v:false| then tasks complete
            functions are not moved, they'll be deleted with group file: >
                :Diantre move diantre group <context> <group> <newcontext>
                <newgroup> [<complete>]
<
    copy:                                                          *group-copy* 
            Like |group-move| but don't delete the group file after moving
            tasks from group <group> to <newgroup> from context <context> to
            <newcontext>. If the optional parameter <complete> is given and
            strictly equals to |v:false| then tasks complete functions are not
            moved: >
                :Diantre copy diantre group <context> <group> <newcontext>
                <newgroup> [<complete>]
<
   merge:                                                         *group-chdir* 
            It renames a list of groups given as variables arguments, each
            task completion function is renamed too. >
                :Diantre merge diantre group <context> <group> <var-args>
<
Note: |group-rename|, |group-chdir|, |group-move|, |group-copy|, |group-merge|
will rename tasks to versionned tasks if they already exists in the
destination group. see |TasksVersions| for mor information.

-------------------------------------------------------------------------------
Tasks related to *PresetGroups* :

     set:                                                            *group-set*
            Set global variable g:diantre_groups which is used to preset
            groups when looking for a task. >
                :Diantre set diantre group <var-args>
<           Example: >
                :Diantre set diantre group task group context
                :Diantre merge diantre context0 group0 group1 group2
                :Diantre new diantre task1 context1 group1
<          Because "task" group was set first, the last command will call: >
                :Diantre new diantre task context1 group1 task1
<          The second command will find "merge" task inside diantre#group only
          and merge group1 and group2 with group0.

          Note: except if you know what you're doing it's not recommanded to set
          global groups for diantre itself. The example above, alter the
          behaviour of task resolver and will corrupt some diantre commands,
          included those to restore group variables. In case it happens you
          can restart vim or use internal function defined in
          plugin/diantre.vim: >
                :call Diantre_set_groups()
                :echo Diantre_groups() 
<
    bset:                                                           *group-bset*
            Set buffer variable b:diantre_groups which is used to preset
            groups when looking for a task only for the current buffer. >
                :Diantre bset diantre group <var-args>
<
  insert:                                                         *group-insert*
 binsert:                                                        *group-binsert*
            Prepend list of global (insert) or buffer (binsert) group preset 
            variables. >
                :Diantre insert diantre group <var-args>
                :Diantre binsert diantre group <var-args>
<
  append:                                                         *group-append*
 bappend:                                                        *group-bappend*
            Append at the end of the list global (append) or buffer (bappend)
            group preset variables. >
                :Diantre append diantre group <var-args>
                :Diantre bappend diantre group <var-args>
<
   unset:                                                          *group-unset*
  bunset:                                                         *group-bunset*
            If no parameter is given it will empty global (unset) or buffer
            (bunset) group preset variables. With <var-args> it removes only
            the given groups: >
                :Diantre unset diantre group [<var-args>]
                :Diantre bunset diantre group [<var-args>]
<
-------------------------------------------------------------------------------
2.4 Contexts                                                  *DiantreContexts*

new:                                                               *context-new*
            Create a new directory <context> in |DiantreHome|/autoload with
            the given name. The directory is made recursively. >
                :Diantre new diantre context <context>
<
remove:                                                         *context-remove*
remove_empty:                                             *context-remove_empty*
            Delete the directory |DiantreHome|/autoload/<context> and all its
            content. Use |context-remove_empty| to remove this directory only
            if it contains no vim files (other type of file are not detected) >
                :Diantre remove diantre context <context>
                :Diantre remove_empty diantre context <context>
<
move:                                                             *context-move*
            Call |group-chdir| for each group inside <context> to move them to
            <newcontext>: >
                :Diantre move diantre context <context> <newcontext>
<
copy:                                                             *context-copy*
            Call |group-copy| for each group inside <context> to copy them to
            <newcontext>: >
                :Diantre copy diantre context <context> <newcontext>
<
-------------------------------------------------------------------------------
Tasks related to *PresetContexts* :

     set:                                                          *context-set*
            Set global variable g:diantre_contexts which is used to preset
            contexts when looking for a task. >
                :Diantre set diantre context <var-args>
<
            Note: By default 'diantre' is set as a context, which remove the
            necessity to type it for each command shown in this documentation.
            For example previous command can be done with: >
                :Diantre set context <var-args>
<
    bset:                                                         *context-bset*
            Set buffer variable b:diantre_contexts which is used to preset
            contexts when looking for a task only for the current buffer. >
                :Diantre bset diantre context <var-args>
<
  insert:                                                       *context-insert*
 binsert:                                                      *context-binsert*
            Prepend list of global (insert) or buffer (binsert) context preset 
            variables. >
                :Diantre insert diantre context <var-args>
                :Diantre binsert diantre context <var-args>
<
  append:                                                       *context-append*
 bappend:                                                      *context-bappend*
            Append at the end of the list global (append) or buffer (bappend)
            context preset variables. >
                :Diantre append diantre context <var-args>
                :Diantre bappend diantre context <var-args>
<
   unset:                                                        *context-unset*
  bunset:                                                       *context-bunset*
            If no parameter is given it will empty global (unset) or buffer
            (bunset) context preset variables. With <var-args> it removes only
            the given contexts: >
                :Diantre unset diantre context [<var-args>]
                :Diantre bunset diantre context [<var-args>]

===============================================================================
3. Configuration                                         *DiantreConfiguration*
-------------------------------------------------------------------------------
3.1 Home Directory                                                *DiantreHome*

Diantre home directory is the directory where it's installed. Each task is
looked in diantre/autoload directory. In previous versions it was configurable
but it's now removed, to simplify and permit the creation of plugins.

-------------------------------------------------------------------------------
3.2 Presets contexts and groups                                        *Presets*

Diantre able to predefine contexts and groups to shorter commands with
presets. See related tasks at |PresetContexts| and |PresetGroups|.
This is the case for diantre itself which is listed in default contexts
presets, and permit to do: >
    :Diantre new task taskname taskcontext taskgroup
<instead of: >
    :Diantre new diantre task taskname taskcontext taskgroup
<
This feature was made to be used with |autocommand|.
It's recommanded to use autocommand groups (|augroup|) and buffer version of
diantre presets. 

A typic example of setting context is the following: >
    augroup python
        autocmd!
        autocmd FileType python Diantre binsert context python
    augroup END
<
You can check defined contexts with internal method Diantre_all_contexts(): >
    :echo Diantre_all_contexts()
<It will show ['python', 'diantre'] if the active buffer is a python file and
only ['diantre'] if it's another type of file. The '_all_' method indicate
buffer and global presets are merged and this is the function used internally
to resolve task names.

You can do the same for groups here a complete example, if you have a context
"vim", a group "test" with for example task "run" then by defining this: >
    augroup vimspec
        autocmd!
        autocmd FileType vimspec Diantre binsert context vim
        autocmd FileType vimspec Diantre binsert group test
    augroup END
<Instead of typing: >
    :Diantre run vim test
<You'll be able to do the same with: >
    :Diantre run
<
You can check defined groups with internal method Diantre_all_groups(): >
    :echo Diantre_all_groups()
<
It will show ['test'] if the active buffer is a vimspec file and an empty list
if it's another type of file like vimscript. The '_all_' method has the same
purpose than for contexts.

-------------------------------------------------------------------------------
3.4 Task versions                                               *TasksVersions*

When you're moving, copying... tasks or groups, sometimes the destination task
function already exists, or you may want to have 2 differents behaviours for
the same task name, this is why task versions exists. It prevents name
collision during some operations by adding double underscore followed by the
last version of the task. An example is more explicit.

Imagine you have a context "ruby" and a group "test" which contains a task
"run". Ruby has a lot of testing frameworks, you can eventually add parameters
to specify which one you want to pick, but it means you'll have to test the
parameter inside your task function, and modify the task everytime you want to
add a new framework. 
So a better solution is to have versioned tasks, let's say your favorite test
framework is rspec, but occasionely you use test-unit. You write a function
ruby#test#run() which launch rspec runner, and a versionned version
ruby#test#run__1() which is going to launch test-unit.
This command will call rspec runner: >
    :Diantre run ruby test
<This one will call test-unit runner: >
    :Diantre run__1 ruby test
<
The task name 'run__1' is not really explicit and having a separate group for
rspec and test-unit is definitly a better option, but this option doesn't
exclude having a generic test group with versionned tasks that call the run
task in specifics groups, because task versions provide an abstraction
comparable to polymorphism. You can choose which version to launch by defining
a global variable with the name of the task, followed by "#version" and so if
you set g:ruby#test#run#version to 1, unit-test runner will be used by
default: >
    :let g:ruby#test#run#version=1
    :Diantre run ruby test
<Is equivalent to: >
    :Diantre run__1 ruby test
<
This feature is particularly usefull when you combine tasks together, for
example if you have a deploy process which consists in testing first then
send your code on a server, the task "deploy" in "production" group can call
the generic task "run" without worrying about the test framework you're using.
>
    function! ruby#production#deploy(server)
        Diantre run ruby test
        Diantre push remote rsync a:server
    endfunction

If you want to call the function directly to get the result for example, you
can use DiantreFunctionName: >

    function! ruby#production#deploy(server)
        let l:testrunner = DiantreFunctionName('ruby', 'test', 'run')
        if call(l:testrunner, [])
            Diantre push remote rsync a:server
        endif
    endfunction
<
===============================================================================
4. License                                                     *DiantreLicense*

MIT License

Copyright (c) 2020 Grégory Salvan apieum[at]gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next
paragraph) shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
