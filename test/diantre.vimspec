let s:scope = themis#helper('scope')
let s:diantre = s:scope.funcs('plugin/diantre/complete.v')

describe Diantre command
   after
       call Diantre_unset_buffer_contexts()
       call Diantre_unset_contexts()
       call Diantre_unset_buffer_groups()
       call Diantre_unset_groups()
       unlet! g:taskcontext#taskgroup#task#version
    end
    context completion
        before
            so test/taskcontext/taskgroup.vim
        end
        after
            Diantre remove diantre context context
            Diantre remove diantre context taskcontext
            call Diantre_unset_contexts()
        end
        it returns tasks from autoload directory
            Diantre new diantre task task context group
            let l:expected = split(s:diantre.diantre_complete('', 'Diantre ', 8), "\n")
            Assert Truthy(match(l:expected, '^task $'))
        end
        it returns tasks from autoload directory with partial task name
            Diantre new diantre task task context group
            let l:expected = split(s:diantre.diantre_complete('ta', 'Diantre ta', 10), "\n")
            Assert Truthy(match(l:expected, '^task $'))
        end
        it returns context from autoload directory
            Diantre new diantre task task context group
            let l:expected = s:diantre.diantre_complete('', 'Diantre task ', 13)
            Assert Equals('context ', l:expected)
        end
        it returns contexts from autoload directory
            Diantre set diantre context nocontext
            Diantre new diantre task new context group
            let l:expected = split(s:diantre.diantre_complete('', 'Diantre new ', 12), "\n")
            Assert Equals(['context ', 'diantre '], l:expected)
        end
        it returns contexts from autoload directory with partial context name
            Diantre new diantre task task context group
            let l:expected = s:diantre.diantre_complete('cont', 'Diantre task cont', 17)
            Assert Equals('context ', l:expected)
        end
        it returns a group within context and task
            Diantre new diantre task task context group
            let l:expected = s:diantre.diantre_complete('', 'Diantre task context ', 21)
            Assert Equals('group ', l:expected)
        end
        it returns a group within context and task and partial group
            let l:expected = split(s:diantre.diantre_complete('t', 'Diantre new diantre t', 21), "\n")
            Assert Equals(['complete ', 'context ', 'group ', 'task '], l:expected)
        end
        it returns groups if context environment variable is set when a task is given
            Diantre new diantre task new context group
            Diantre set diantre context diantre
            let l:expected = split(s:diantre.diantre_complete('', 'Diantre new ', 12), "\n")
            Assert Equals(sort(['complete ', 'context ', 'group ', 'task ', 'diantre ']), l:expected)
        end
        it calls task function name added with __complete to complete task parameters 
            Diantre new diantre task task1 taskcontext taskgroup
            let l:expected = s:diantre.diantre_complete('', 'Diantre task1 taskcontext taskgroup ', 36)
            Assert Equals("arg1 \narg2 ", l:expected)
        end
        it calls parameter __complete if context environment var is defined 
            Diantre new diantre task task1 taskcontext taskgroup
            Diantre set diantre context taskcontext
            let l:expected = s:diantre.diantre_complete('', 'Diantre task1 taskgroup ', 24)
            Assert Equals("arg1 \narg2 ", l:expected)
        end
        it calls parameter __complete if group environment var is defined 
            Diantre new diantre task task1 taskcontext taskgroup
            Diantre set diantre group taskgroup
            let l:expected = s:diantre.diantre_complete('', 'Diantre task1 taskcontext ', 26)
            Assert Equals("arg1 \narg2 \ntaskgroup ", l:expected)
        end
        it calls parameter __complete if context and group environment var are defined 
            Diantre new diantre task task1 taskcontext taskgroup
            Diantre set diantre context taskcontext
            Diantre set diantre group taskgroup
            let l:expected = s:diantre.diantre_complete('', 'Diantre task1 ', 14)
            Assert Equals("arg1 \narg2 \ntaskcontext \ntaskgroup ", l:expected)
        end
        it returns nothing if cursor position is at the end of Diantre 
            Diantre new diantre task new context group
            let l:expected = s:diantre.diantre_complete('Diantre', 'Diantre new', 8)
            Assert Equals('', l:expected)
        end
        context cursor index for completion
            it returns normal split if cursor is at the end
                let l:expected = s:diantre.split_cmd_line('', 'Diantre new ', 12)
                Assert Equals([2, ['Diantre', 'new', '']], l:expected)
            end
            it returns split with inserted arg_lead at correct position if arg_lead is empty
                let l:expected = s:diantre.split_cmd_line('', 'Diantre new  task', 13)
                Assert Equals([2, ['Diantre', 'new', '', 'task']], l:expected)
            end
            it returns normal split with correct index if no space and not at the end
                let l:expected = s:diantre.split_cmd_line('Diantre', 'Diantre new', 10)
                Assert Equals([1, ['Diantre', 'new']], l:expected)
            end
        end
    end
    context a task is a function
        after
            Diantre remove diantre context taskcontext
            call g:diantre.del_context('taskcontext')
            call vmock#clear()
        end
        it calls function given by cache
            let l:called = v:false
            function Testtask1() closure
                let l:called = v:true
            endfunction
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            Diantre task1 taskcontext taskgroup
            Assert Truthy(l:called)
        end
        it calls function with arguments if given
            function Testtask2(a1, a2, a3) 
                return v:true
            endfunction
            call g:diantre.add_task('task2', 'taskcontext', 'taskgroup', 'Testtask2')
            call vmock#mock('Testtask2').with('arg1', 'arg2', 'arg3').once()
            Diantre task2 taskcontext taskgroup arg1 arg2 arg3
            call vmock#verify()
        end
        it calls versioned version of tasks
            call g:diantre.add_task('task__2', 'taskcontext', 'taskgroup', 'Testtask1')
            call vmock#mock('Testtask1').once()
            let g:taskcontext#taskgroup#task#version=2
            Diantre task taskcontext taskgroup
            call vmock#verify()
        end
        it can use context from buf variable
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_buffer_contexts('taskcontext')
            Diantre task1 taskgroup
            call vmock#verify()
        end
        it gets context from buf variable list for the first existing task
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_buffer_contexts('nonexistingcontext', 'taskcontext')
            Diantre task1 taskgroup
            call vmock#verify()
        end
        it can use context from global variable
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_contexts('taskcontext')
            Diantre task1 taskgroup
            call vmock#verify()
        end
        it gets context from global variable list for the first existing task
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_contexts('nonexistingcontext', 'taskcontext')
            Diantre task1 taskgroup
            call vmock#verify()
        end
        it use buffer variable defined context first
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_buffer_contexts('taskcontext')
            call Diantre_set_contexts('taskcontext1')
            Diantre task1 taskgroup
            call vmock#verify()
        end
        it use context only if task exists 
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_contexts('context1')
            Diantre task1 taskcontext taskgroup
            call vmock#verify()
        end
        it can use group from buf variable without context variable
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_buffer_groups('taskgroup')
            Diantre task1 taskcontext
            call vmock#verify()
        end
        it can use group from buf variable with context variable
            call vmock#mock('Testtask1').once()
            call g:diantre.add_task('task1', 'taskcontext', 'taskgroup', 'Testtask1')
            call Diantre_set_contexts('taskcontext')
            call Diantre_set_buffer_groups('taskgroup')
            Diantre task1
            call vmock#verify()
        end
        it throws an error with task name when it doesn't exist
            Throws /task notcontext group/ execute('Diantre task notcontext group')
        end
        it throws error with all available contexts and groups for task
            call g:diantre.add_task('task', 'bcontext', 'group', 'Testtask1')
            call g:diantre.add_task('task', 'gcontext', 'group', 'Testtask1')
            Throws /task group context/  execute('Diantre task group context')
            try
                Diantre task group context
            catch
                let message = split(v:exception, "\n")
                Assert Equals("\ttask bcontext group", message[2])
                Assert Equals("\ttask gcontext group", message[3])
            endtry
            call g:diantre.del_context('bcontext')
            call g:diantre.del_context('gcontext')
        end
    end
end
